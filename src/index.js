export { Head, Redirect, Language, Link } from '@sckit/gatsby-i18n';

export { default as withI18next } from './withI18next';
