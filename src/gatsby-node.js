export {
  onPreBootstrap,
  onCreateNode,
  onPreExtractQueries,
  onCreatePage,
} from '@sckit/gatsby-i18n/plugin';
